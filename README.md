A git command for creating and maintaining archives of local git branches.

## Examples

This addition to git is for those that have too many branches that clutter the output of `git branch`

``` sh
$ git branch
feature_1_current
feature_1_snapshot_1
feature_1_snapshot_2
# [...]
feature_2_current
feature_2_snapshot_1
feature_2_snapshot_2
# [...]
```

With `git archive-branch` you can hide branches from view so you can see those that actually are important.

```sh
$ git archive-branch -a feature_1_snapshot_* feature_2_snapshot_*
$ git branch
feature_1_current
feature_2_current
```

The archived branches and the commits they reference are still known to git. You can view the list at any time.

```sh
$ git archive-branch
feature_1_snapshot_1
feature_1_snapshot_2
# [...]
feature_2_snapshot_1
feature_2_snapshot_2
# [...]
```

If you need to convert archived branches back to normal branches, you can do so at any time.

```sh
$ git archive-branch -r feature_1_snapshot_*
$ git branch
feature_1_current
feature_1_snapshot_1
feature_1_snapshot_2
# [...]
feature_2_current
```

When you decide you no longer need an archived branch, you can delete them.

```sh
$ git archive-branch -D feature_2_snapshot_1 feature_2_snapshot_2 ...
```

Finally, just as you can uses `git branch feature_3` to create a new branch for your current `HEAD` commit, you can do the same thing to create a new archived branch immediately.

```sh
$ git archive-branch feature_3_snapshot_1
$ git archive-branch
feature_3_snapshot_1
```

## Installation

Symlink (or copy) the git-archive-branch.sh script without the extension to a 
directory in your path:

    # Assumes $HOME/bin is in your path
    $ ln -s path/to/git-archive-branch.sh $HOME/bin/git-archive-branch
