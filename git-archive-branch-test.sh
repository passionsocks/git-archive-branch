#!/bin/sh
# (checked by https://github.com/koalaman/shellcheck)

# ---------------------------------------------------------------------------
# (License: Apache-2.0)
#
# Copyright 2019 Lloyd Pique
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ---------------------------------------------------------------------------

# ---------------------------------------------------------------------------
# Test coverage for git-archive-branch.sh
# ---------------------------------------------------------------------------

TMPDIR=git-archive-branch-test-tmp

# Allows tput to silently fail (CI server)
quiet_tput() {
	tput "$@" 2>/dev/null
}

# Prints a failure message to stderr and exits.
fail() {
	quiet_tput bold
	quiet_tput setaf 1
	echo "$*"
	quiet_tput sgr0

	exit 1
}

success() {
	quiet_tput bold
	quiet_tput setaf 2
	echo "All tests pass"
	quiet_tput sgr0
}

# Runs a command. On failure, prints a message and exits with an error.
checked_run() {
	if ! "$@"; then
		fail "FAIL   $*"
	fi
}

expect_fail() {
	if "$@"; then
		fail "DID NOT FAIL: $*"
	fi
}

test() {
	quiet_tput bold
	quiet_tput setaf 4
	echo "trying $1"
	quiet_tput sgr0

	"$@"

	quiet_tput bold
	quiet_tput setaf 2
	echo "PASS   $1"
	quiet_tput sgr0
}

make_branch_and_get_hash() {
	checked_run touch "${TMPDIR}/${1}.txt"
	checked_run git -C "${TMPDIR}" add "${1}.txt" >/dev/null
	checked_run git -C "${TMPDIR}" commit -a -m '"dummy"' >/dev/null
	checked_run git -C "${TMPDIR}" branch "${1}" >/dev/null
	checked_run git -C "${TMPDIR}" show-ref --verify -s "refs/heads/${1}"
}

setup() {
	checked_run rm -rf "$TMPDIR"
	checked_run mkdir "$TMPDIR"
	checked_run git -C "$TMPDIR" init >/dev/null
	checked_run git -C "$TMPDIR" config user.email "null@example.com"
	checked_run git -C "$TMPDIR" config user.name "Nobody"

	HASH_BASE=$(make_branch_and_get_hash base)
	HASH_FEATURE_1=$(make_branch_and_get_hash feature_1)
	HASH_FEATURE_1_SNAPSHOT_1=$(make_branch_and_get_hash feature_1_snapshot_1)
	HASH_FEATURE_1_SNAPSHOT_2=$(make_branch_and_get_hash feature_1_snapshot_2)
	HASH_FEATURE_2=$(make_branch_and_get_hash feature_2)
	HASH_FEATURE_2_SNAPSHOT_1=$(make_branch_and_get_hash feature_2_snapshot_1)
	HASH_FEATURE_2_SNAPSHOT_2=$(make_branch_and_get_hash feature_2_snapshot_2)

	checked_run git -C "$TMPDIR" checkout base
	checked_run git -C "$TMPDIR" branch -D master
}

teardown() {
	checked_run rm -rf "$TMPDIR"
}

assert_head() {
	git -C "$TMPDIR" show-ref --verify HEAD	> $TMPDIR/ACTUAL
	if ! diff -U 1 --label ACTUAL --label EXPECTED $TMPDIR/ACTUAL - ; then
		fail "Git HEAD does not match expected value."
	fi
}

assert_refs() {
	git -C "$TMPDIR" for-each-ref --format "%(objectname) %(refname)" > $TMPDIR/ACTUAL
	if ! diff -U 1 --label ACTUAL --label EXPECTED $TMPDIR/ACTUAL - ; then
		fail "Git refs do not match expected values."
	fi
}

git_archive_branch() {
	if ! (cd "$TMPDIR" && sh ../git-archive-branch.sh "$@") ; then
		fail "git-archive-branch.sh $*"
	fi
}

git_archive_branch_expect_fail() {
	if (cd "$TMPDIR" && sh ../git-archive-branch.sh "$@") ; then
		fail "DID NOT FAIL: git-archive-branch.sh $*"
	fi
}

archive_one_branch() {
	git_archive_branch -a feature_2

	assert_head <<-EOF
		${HASH_BASE} HEAD
EOF

	assert_refs <<-EOF
		${HASH_BASE} refs/heads/base
		${HASH_FEATURE_1} refs/heads/feature_1
		${HASH_FEATURE_1_SNAPSHOT_1} refs/heads/feature_1_snapshot_1
		${HASH_FEATURE_1_SNAPSHOT_2} refs/heads/feature_1_snapshot_2
		${HASH_FEATURE_2_SNAPSHOT_1} refs/heads/feature_2_snapshot_1
		${HASH_FEATURE_2_SNAPSHOT_2} refs/heads/feature_2_snapshot_2
		${HASH_FEATURE_2} refs/my-branch-archives/feature_2
EOF
}

restore_one_branch() {
	git_archive_branch -r feature_2

	assert_head <<-EOF
		${HASH_BASE} HEAD
EOF

	assert_refs <<-EOF
		${HASH_BASE} refs/heads/base
		${HASH_FEATURE_1} refs/heads/feature_1
		${HASH_FEATURE_1_SNAPSHOT_1} refs/heads/feature_1_snapshot_1
		${HASH_FEATURE_1_SNAPSHOT_2} refs/heads/feature_1_snapshot_2
		${HASH_FEATURE_2} refs/heads/feature_2
		${HASH_FEATURE_2_SNAPSHOT_1} refs/heads/feature_2_snapshot_1
		${HASH_FEATURE_2_SNAPSHOT_2} refs/heads/feature_2_snapshot_2
EOF
}

archive_two_branches() {
	git_archive_branch -a feature_1 feature_2

	assert_head <<-EOF
		${HASH_BASE} HEAD
EOF

	assert_refs <<-EOF
		${HASH_BASE} refs/heads/base
		${HASH_FEATURE_1_SNAPSHOT_1} refs/heads/feature_1_snapshot_1
		${HASH_FEATURE_1_SNAPSHOT_2} refs/heads/feature_1_snapshot_2
		${HASH_FEATURE_2_SNAPSHOT_1} refs/heads/feature_2_snapshot_1
		${HASH_FEATURE_2_SNAPSHOT_2} refs/heads/feature_2_snapshot_2
		${HASH_FEATURE_1} refs/my-branch-archives/feature_1
		${HASH_FEATURE_2} refs/my-branch-archives/feature_2
EOF
}

restore_two_branches() {
	git_archive_branch -r feature_1 feature_2

	assert_head <<-EOF
		${HASH_BASE} HEAD
EOF

	assert_refs <<-EOF
		${HASH_BASE} refs/heads/base
		${HASH_FEATURE_1} refs/heads/feature_1
		${HASH_FEATURE_1_SNAPSHOT_1} refs/heads/feature_1_snapshot_1
		${HASH_FEATURE_1_SNAPSHOT_2} refs/heads/feature_1_snapshot_2
		${HASH_FEATURE_2} refs/heads/feature_2
		${HASH_FEATURE_2_SNAPSHOT_1} refs/heads/feature_2_snapshot_1
		${HASH_FEATURE_2_SNAPSHOT_2} refs/heads/feature_2_snapshot_2
EOF
}

delete_one_archived_branch() {
	git_archive_branch -a feature_2
	git_archive_branch -D feature_2

	assert_head <<-EOF
		${HASH_BASE} HEAD
EOF

	assert_refs <<-EOF
		${HASH_BASE} refs/heads/base
		${HASH_FEATURE_1} refs/heads/feature_1
		${HASH_FEATURE_1_SNAPSHOT_1} refs/heads/feature_1_snapshot_1
		${HASH_FEATURE_1_SNAPSHOT_2} refs/heads/feature_1_snapshot_2
		${HASH_FEATURE_2_SNAPSHOT_1} refs/heads/feature_2_snapshot_1
		${HASH_FEATURE_2_SNAPSHOT_2} refs/heads/feature_2_snapshot_2
EOF
}

create_one_archived_branch() {
	checked_run git -C "$TMPDIR" checkout --quiet --detach "${HASH_FEATURE_2}"
	git_archive_branch feature_2

	assert_head <<-EOF
		${HASH_FEATURE_2} HEAD
EOF

	assert_refs <<-EOF
		${HASH_BASE} refs/heads/base
		${HASH_FEATURE_1} refs/heads/feature_1
		${HASH_FEATURE_1_SNAPSHOT_1} refs/heads/feature_1_snapshot_1
		${HASH_FEATURE_1_SNAPSHOT_2} refs/heads/feature_1_snapshot_2
		${HASH_FEATURE_2_SNAPSHOT_1} refs/heads/feature_2_snapshot_1
		${HASH_FEATURE_2_SNAPSHOT_2} refs/heads/feature_2_snapshot_2
		${HASH_FEATURE_2} refs/my-branch-archives/feature_2
EOF

	checked_run git -C "$TMPDIR" checkout base
}

archive_current_branch() {
	checked_run git -C "$TMPDIR" checkout feature_1
	git_archive_branch -a feature_1

	assert_head <<-EOF
		${HASH_FEATURE_1} HEAD
EOF

	assert_refs <<-EOF
		${HASH_BASE} refs/heads/base
		${HASH_FEATURE_1_SNAPSHOT_1} refs/heads/feature_1_snapshot_1
		${HASH_FEATURE_1_SNAPSHOT_2} refs/heads/feature_1_snapshot_2
		${HASH_FEATURE_2_SNAPSHOT_1} refs/heads/feature_2_snapshot_1
		${HASH_FEATURE_2_SNAPSHOT_2} refs/heads/feature_2_snapshot_2
		${HASH_FEATURE_1} refs/my-branch-archives/feature_1
		${HASH_FEATURE_2} refs/my-branch-archives/feature_2
EOF

	checked_run git -C "$TMPDIR" checkout base
}

archive_branch_pattern() {
	git_archive_branch -a feature_1*

	assert_head <<-EOF
		${HASH_BASE} HEAD
EOF

	assert_refs <<-EOF
		${HASH_BASE} refs/heads/base
		${HASH_FEATURE_2_SNAPSHOT_1} refs/heads/feature_2_snapshot_1
		${HASH_FEATURE_2_SNAPSHOT_2} refs/heads/feature_2_snapshot_2
		${HASH_FEATURE_1} refs/my-branch-archives/feature_1
		${HASH_FEATURE_1_SNAPSHOT_1} refs/my-branch-archives/feature_1_snapshot_1
		${HASH_FEATURE_1_SNAPSHOT_2} refs/my-branch-archives/feature_1_snapshot_2
		${HASH_FEATURE_2} refs/my-branch-archives/feature_2
EOF
}

restore_branch_pattern() {
	git_archive_branch -r feature_1_snapshot*

	assert_head <<-EOF
		${HASH_BASE} HEAD
EOF

	assert_refs <<-EOF
		${HASH_BASE} refs/heads/base
		${HASH_FEATURE_1_SNAPSHOT_1} refs/heads/feature_1_snapshot_1
		${HASH_FEATURE_1_SNAPSHOT_2} refs/heads/feature_1_snapshot_2
		${HASH_FEATURE_2_SNAPSHOT_1} refs/heads/feature_2_snapshot_1
		${HASH_FEATURE_2_SNAPSHOT_2} refs/heads/feature_2_snapshot_2
		${HASH_FEATURE_1} refs/my-branch-archives/feature_1
		${HASH_FEATURE_2} refs/my-branch-archives/feature_2
EOF
}

archive_unknown_branch_fails() {
	git_archive_branch_expect_fail -a feature_xxx
}

restore_unknown_branch_fails() {
	git_archive_branch_expect_fail -r feature_xxx
}

delete_unknown_branch_fails() {
	git_archive_branch_expect_fail -D feature_xxx
}

test setup
test archive_one_branch
test restore_one_branch
test archive_two_branches
test restore_two_branches
test delete_one_archived_branch
test create_one_archived_branch
test archive_current_branch
test archive_branch_pattern
test restore_branch_pattern
test archive_unknown_branch_fails
test restore_unknown_branch_fails
test delete_unknown_branch_fails
test teardown
success
exit 0
