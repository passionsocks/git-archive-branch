#!/bin/sh
# (checked by https://github.com/koalaman/shellcheck)

# ---------------------------------------------------------------------------
# (License: Apache-2.0)
#
# Copyright 2019 Lloyd Pique
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ---------------------------------------------------------------------------

# ---------------------------------------------------------------------------
# git-archive-branch.sh -- Archives local branches to declutter "git branch"
# ---------------------------------------------------------------------------

print_usage() {
	echo "Usage: git archive-branch"
	echo "   or: git archive-branch <new-archive-branch-name> "
	echo "   or: git archive-branch -a <pattern> [...] "
	echo "   or: git archive-branch -r <pattern> [...]"
	echo "   or: git archive-branch -D <archive-branch-name>"
	echo ""
	echo "    -a      convert matching local branches to archive branches"
	echo "    -r      restore matching archive branches to local branches"
	echo "    -D      delete an archive branch"
	echo ""
	echo "When run with no arguments the list archive branches is output."
}

# Archives are maintained using this git reference prefix path.
REF_ARCHIVE_PREFIX=refs/my-branch-archives

# Prints a failure message to stderr and exits.
fail() {
	echo "$*" 1>&2
	exit 1
}

# Checks that the reference in the first argument is a known git reference.
# Otherwise prints the second argument and exits with an error code.
assert_ref_exists() {
	if ! git show-ref --verify -s "${1}" 2>/dev/null >/dev/null; then
		fail "$2"
	fi
}

# Checks that the reference in the first argument is not a known git reference.
# Otherwise prints the second argument and exits with an error code.
assert_ref_unknown() {
	if git show-ref --verify -s "${1}" 2>/dev/null >/dev/null; then
		fail "$2"
	fi
}

# Checks that the argument names a known local branch.
assert_is_local_branch() {
	assert_ref_exists "refs/heads/${1}" \
		"${1} is not a local branch"
}

# Checks that the argument does not name a known local branch.
assert_is_not_local_branch() {
	assert_ref_unknown "refs/heads/${1}" \
		"${1} already exists as a local branch"
}

# Checks that the argument names a known archive branch.
assert_is_archive_branch() {
	assert_ref_exists "${REF_ARCHIVE_PREFIX}/${1}" \
		"${1} is not an archive branch"
}

# Checks that the argument does not name a known archive branch.
assert_is_not_archive_branch() {
	assert_ref_unknown "${REF_ARCHIVE_PREFIX}/${1}" \
		"${1} already exists as an archive branch"
}

# Runs a command. On failure, prints a message and exits with an error.
checked_run() {
	if ! "$@"; then
		fail "Failed: $*"
	fi
}

# Lists matching refs
list_matching_refs() {
	checked_run git for-each-ref --format='%(refname:lstrip=2)' "$1"
}

# Lists the names of all archive branches.
list_archive_branches() {
	list_matching_refs "${REF_ARCHIVE_PREFIX}"
}

# Returns the full hash for a commit given a reference.
get_full_hash_for_ref() {
	checked_run git show-ref --verify -s "${1}"
}

# Returns the short hash for a commit given a reference.
get_short_hash_for_ref() {
	checked_run git show-ref --verify --abbrev -s "${1}"
}

# Creates an archive branch for the current commit, altering nothing else
create_archive_branch() {
	branch="$1"
	assert_is_not_archive_branch "${branch}"

	# Create the archive branch.
	archive_branch_ref="${REF_ARCHIVE_PREFIX}/${branch}"
	hash=$(get_full_hash_for_ref HEAD)
	checked_run git update-ref "${archive_branch_ref}" "${hash}"

	echo "Created archive branch ${branch}"
}

# Converts a local branch to an archive branch, removing the local branch.
convert_local_branch_to_archive_branch() {
	branch="$1"
	assert_is_local_branch "${branch}"
	assert_is_not_archive_branch "${branch}"

	branch_ref=refs/heads/${branch}
	hash=$(get_full_hash_for_ref "${branch_ref}")

	# Create the archive branch.
	archive_branch_ref="${REF_ARCHIVE_PREFIX}/${branch}"
	checked_run git update-ref "${archive_branch_ref}" "${hash}"

	# If we are archiving the currently checked-out branch,
	# detach the checkout.
	if [ "${hash}" = "$(get_full_hash_for_ref HEAD)" ] ; then
		checked_run git checkout --quiet --detach >/dev/null
	fi

	# Delete the local branch.
	checked_run git branch -D "${branch}" >/dev/null

	echo "Archived local branch ${branch}"
}

# Converts local branches to archive branches, removing the local branches.
convert_local_to_branch_pattern_to_archive_branches() {
	matches=$(list_matching_refs "refs/heads/${1}")
	if [ -z "$matches" ]; then
		fail "${1} does not match any local branch"
	fi
	for match in $matches; do
		convert_local_branch_to_archive_branch "$match"
	done
}

# Converts local branches to archive branches, removing the local branches.
convert_local_to_branches_to_archive_branches() {
	while [ -n "$1" ]; do
		convert_local_to_branch_pattern_to_archive_branches "$1"
		shift
	done
}

# Deletes an archive branch.
delete_archive_branch() {
	branch="$1"
	assert_is_archive_branch "${branch}"

	archive_branch_ref="${REF_ARCHIVE_PREFIX}/${branch}"
	hash=$(get_full_hash_for_ref "${archive_branch_ref}")
	short_hash=$(get_short_hash_for_ref "${archive_branch_ref}")

	# Delete the archive branch.
	checked_run git update-ref -d "${archive_branch_ref}" "${hash}"

	# Like "git branch -D", we print the shortened hash.
	echo "Deleted archive branch ${branch} (was ${short_hash})"
}

# Deletes archive branches.
delete_archive_branches() {
	while [ -n "$1" ]; do
		delete_archive_branch "$1"
		shift
	done
}

# Converts an archive branch to a local branch, removing the archive branch.
restore_archive_branch() {
	branch="$1"
	assert_is_not_local_branch "${branch}"
	assert_is_archive_branch "${branch}"

	archive_branch_ref="${REF_ARCHIVE_PREFIX}/${branch}"
	hash=$(get_full_hash_for_ref "${archive_branch_ref}")

	# Create the local branch
	branch_ref="refs/heads/${branch}"
	checked_run git update-ref "${branch_ref}" "${hash}"

	# Delete the archive branch
	checked_run git update-ref -d "${archive_branch_ref}" "${hash}"

	echo "Created local branch ${branch}"
}

# Converts local branches to archive branches, removing the local branches.
restore_archive_branch_for_pattern() {
	matches=$(list_matching_refs "${REF_ARCHIVE_PREFIX}/${1}")
	if [ -z "$matches" ]; then
		fail "${1} does not match any archive branch"
	fi
	for match in $matches; do
		restore_archive_branch "$match"
	done
}

# Converts archive branches to local branches, removing the archive branches.
restore_archive_branches() {
	while [ -n "$1" ]; do
		restore_archive_branch_for_pattern "$1"
		shift
	done
}

# In general we try to behave like "git branch" where possible.

case "$1" in
    -a) shift; convert_local_to_branches_to_archive_branches "$@"; exit 0 ;;
    -D) shift; delete_archive_branches "$@"; exit 0 ;;
    -r) shift; restore_archive_branches "$@"; exit 0 ;;
    --) shift; ;;
    -*) print_usage; exit 1 ;;
esac

if [ -z "$1" ]; then
	list_archive_branches
	exit 0
fi

if [ -n "$2" ]; then
	print_usage
	exit 1
fi

create_archive_branch "$1"
